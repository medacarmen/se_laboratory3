import java.util.*;
/*
 Modify Flower class from Ciclul de viaţă al obiectelor so that it keeps track of the number of constructed object.
 Add a method which returns the number of constructed objects. Use 'static' variables and methods for implementing this task.
 */

public class Exercise5_Flower{
            int petal;
            static int count=0;
          public Exercise5_Flower(int p){
              petal=p;
              System.out.println("New flower has been created!");
              count++;
            }
    public static int countObjects(){
        return count;
    }
 
    public static void main(String[] args) {
        Exercise5_Flower f1 = new Exercise5_Flower(4);
        Exercise5_Flower f2 = new Exercise5_Flower(6);
      //Flower f3 = new Flower(); //eroare de compilare - stergeti linia pentru a putea rula
       System.out.println(countObjects());
    }
}


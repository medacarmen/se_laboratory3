import java.util.*;

/*
 A class called Author contains:

 Three private instance variables: name (String), email (String), and gender (char of either 'm' or 'f');
 One constructor to initialize the name, email and gender with the given values;
 public Author (String name, String email, char gender) {……}
 (There is no default constructor for Author, as there are no defaults for name, email and gender.)
 public getters/setters: getName(), getEmail(), setEmail(), and getGender();
 (There are no setters for name and gender, as these attributes cannot be changed.)
 A toString() method that returns “author-name (gender) at email”, e.g., “My Name (m) at myemail@somewhere.com”.
 Write the Author class. Also write a test program called TestAuthor to test the constructor and public methods.
 */
public class Exercise3{
    
    public static void main(String[] args){
      
        Author author1 = new Author("J.K.Rowling","harrypotter@hogwarts.owl",'f');
        System.out.println(author1.getName());
        System.out.println(author1.getEmail());
        System.out.println(author1.getGender());
        TestAuthor testAuthor = new TestAuthor();
        
    }
}
    class Author{
        
        private String name, email;
        private char gender;
        public Author(String name, String email, char gender){
            this.name = name;
            this.email = email;
            this.gender = gender;
    
        }
        public String getName(){
            return this.name;
        }
        public String getEmail(){
            return this.email;
        }
        public void setEmail(String email){
            this.email = email;
        }
        public char getGender(){
            return this.gender;
        }
        public String toString(){
            return (this.name + " ( " + this.gender + " ) at " + this.email);
        }
    
    }
    
    class TestAuthor{
     
        public TestAuthor(){
            Author testAuthor = new Author("TestName","test@email.com",'m');
            testAuthor.setEmail("test_email@testing.com");
            System.out.println(testAuthor.toString());
            
        }
    }


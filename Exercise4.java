import java.util.*;

/*
 Exercise 4
 A class called MyPoint, which models a 2D point with x and y coordinates contains:

 Two instance variables x (int) and y (int).
 A “no-argument” (or “no-arg”) constructor that construct a point at (0, 0).
 A constructor that constructs a point with the given x and y coordinates.
 Getter and setter for the instance variables x and y.
 A method setXY() to set both x and y.
 A toString() method that returns a string description of the instance in the format “(x, y)”.
 A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
 An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
 Write the code for the class MyPoint. Also write a test class (called TestMyPoint) to test all the methods defined in the class.
 */

public class Exercise4{
    
    public static void main(String[] args){
        MyPoint p1 = new MyPoint(2,3);
        MyPoint p2 = new MyPoint(5,6);
        System.out.println("p1 = " + p1.toString());
        System.out.println("The distance from p1 to (10, 10) is : " + p1.distance(10,10));
        System.out.println("The distance from p1 to p2 is : " + p1.distance(p2));
        TestMyPoint testPoint = new TestMyPoint();
    }
}
class MyPoint{
    int x, y; //coordinates
    public MyPoint(){
        this.x = 0;
        this.y = 0;
    }
    public MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }
    public void setX(int x){
        this.x = x;
    }
    public void setY(int y){
        this.y = y;
    }
    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }
    public void setXY(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return ("(" + this.x + ", " + this.y + ")");
    }
    public double distance(int x, int y){
        return (Math.sqrt(((x-(double)this.x)*(x-(double)this.x))+((y-(double)this.y)*(y-(double)this.y))));
    }
    public double distance(MyPoint another){
        return (Math.sqrt((((double)another.getX()-(double)this.x)*((double)another.getX()-(double)this.x))+(((double)another.getY()-(double)this.y)*((double)another.getY()-(double)this.y))));
    }
    
}
class TestMyPoint{
    
    public TestMyPoint(){
        MyPoint testPoint1 =  new MyPoint();
        testPoint1.setX(5);
        testPoint1.setY(200);
        System.out.println(testPoint1.toString());
        testPoint1.setXY(10,20);
        MyPoint testPoint2 = new MyPoint(10, 15);
        System.out.println("The distance from the first point to (5, 5) is : " + testPoint1.distance(5,5));
        System.out.println("The distance between the two points is : " + testPoint1.distance(testPoint2));
    }
    
}

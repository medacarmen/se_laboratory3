import java.util.*;

/*
 A class Robot contains:

 One instance variable 'x' (of type int) representing the position of robot;
 One default constructor which initialize the value to 1;
 One change(int k) method which add to the current robot x value to k (as long as k >= 1);
 One toString() method which returns the position of robot;
 Write a class which models the class Robot . Write a class TestRobot which test Robot class.
 */

public class Exercise1{
    
    public static void main(String[] args){
        Robot robot = new Robot();
        System.out.println(robot.toString());
        robot.change(5);
        System.out.println( robot.toString());
        TestRobot testRobot = new TestRobot();
        
        
    }
}

class Robot{
    int x; // position of the robot;
    public Robot(){
        this.x=1;
    }
    public void change(int k){
        this.x = (k >=1 ? k : 0);
    }
    public String toString(){
        return ("The position of the robot is " + this.x);
    }
}

class TestRobot{
    public TestRobot(){
     Robot testRobot = new Robot();
     testRobot.change(50);
     System.out.println( testRobot.toString());
    }
}

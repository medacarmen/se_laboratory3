import java.util.*;

/*
 A class called Circle contains:

 Two private instance variables: radius (of type double) and color (of type String), with default value of 1.0 and “red”, respectively.
 Two overloaded constructors;
 Two public methods: getRadius() and getArea().
 Write a class which models the class Circle. Write a class TestCircle which test Circle class.
 */

public class Exercise2{
    
    public static void main(String[] args){
        
        Circle circle1 = new Circle();
        System.out.println("The radius of the first cirlce: " + circle1.getRadius());
        Circle circle2 = new Circle(10, "green");
        System.out.println("The area of the second circle: " + circle2.getArea());
        TestCircle testCircle = new TestCircle();
    }
}

class Circle{
    static final double PI = 3.141592653589793;
    private double radius = 1.0;
    private String color = "red";
    public Circle(){
        this.radius = 1.0;
        this.color = "red";
    }
    public Circle(double radius, String color){
        this.radius =  radius;
        this.color = color;
    }
    public double getRadius(){
        return (this.radius);
    }
    public double getArea(){
        return (PI * this.radius * this.radius);
        
    }
}

class TestCircle{
    public TestCircle(){
    Circle testCircle = new Circle(5,"blue");
    System.out.println("The radius of the test cirlce: " + testCircle.getRadius());}
}
